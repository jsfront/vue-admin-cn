export default defineAppConfig({
  title: 'vue-admin',
  keywords: 'vue2,vue3,Vue前端,Vue前端管理模板,vue前端UI组件库,vue前端hooks工具集,前端技术开发,javascript技术',
  description: 'Vue前端交流群 - 364912432',
  url: 'vue-admin.cn',
  author: 'admin',
  email: 'jikeytang@163.com',
})
